##After install

Repository don't have vendors and packages, so we can install them manual using next commands

    docker-compose exec application composer install
and

    docker-compose exec application npm i
    
If you don't want run this command every time after container rebuilding - comment 177 and 180 lines in main Dockerfile.
    
##Docker notes

- For running any magento2 commands from docker container, we can use next command

        docker-compose exec application bin/magento <command>
    
- Grunt can be called using
 
        docker-compose exec application grunt <command>
    
- Performance issues connected with the way of sharing files between docker containers. That's why some magento dirs were copied direct to containers. This directories are:
    - app (except design and code, where development is doing)
    - bin
    - lib
    - node_modules
    - phpserver
    - pub
    - var
    - vendor
 
That's why if you want to include changes in this directories, you need to rebuild containers.
        
##XDebug

- **_Only_** for Windows - open port 9000 in firewall (or public network for Idea)
- Create `PHP Remote Debug`
- Don't forget to set directory mappings
- Set Idea key to `docker`
