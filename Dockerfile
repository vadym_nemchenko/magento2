FROM php:7.0-apache

RUN apt-get update && apt-get install -y \
        git \
        imagemagick \
        libcurl4-openssl-dev \
        libfreetype6-dev \
        libicu-dev \
        libjpeg-turbo-progs \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng12-dev \
        libxml2-dev \
        libxslt-dev \
        libz-dev \
        libpq-dev \
        libjpeg-dev \
        libfreetype6-dev \
        libssl-dev \
        ssmtp \
        unzip \
        wget \
        zlib1g-dev \
        libmemcached-dev \
        mc \
    && docker-php-ext-install \
        bcmath \
        curl \
        exif \
        intl \
        mbstring \
        mcrypt \
        pdo_mysql \
        mysqli \
        opcache \
        pcntl \
        pdo_mysql \
        simplexml \
        soap \
        xml \
        xsl \
        zip \
        tokenizer \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install gd

#####################################
# xDebug:
#####################################

# Install the xdebug extension
RUN pecl install xdebug && \
    docker-php-ext-enable xdebug

#####################################
# Human Language and Character Encoding Support:
# Install intl and requirements
#####################################

RUN apt-get install -y \
        zlib1g-dev \
        libicu-dev g++ \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl

#####################################
# GHOSTSCRIPT:
#####################################

# Install the ghostscript extension
# for PDF editing

RUN apt-get install -y \
        poppler-utils \
        ghostscript

#####################################
# LDAP:
#####################################

RUN apt-get install -y \
        libldap2-dev \
    && docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ \
    && docker-php-ext-install ldap


#####################################
# Image optimizers:
#####################################
USER root
RUN apt-get install -y --force-yes \
        jpegoptim \
        optipng \
        pngquant \
        gifsicle

#####################################
# PHP Memcached:
#####################################

RUN \
    # Install the php memcached extension
    curl -L -o /tmp/memcached.tar.gz "https://github.com/php-memcached-dev/php-memcached/archive/php7.tar.gz" \
    && mkdir -p memcached \
    && tar -C memcached -zxvf /tmp/memcached.tar.gz --strip 1 \
    && ( \
        cd memcached \
        && phpize \
        && ./configure \
        && make -j$(nproc) \
        && make install \
    ) \
    && rm -r memcached \
    && rm /tmp/memcached.tar.gz \
    && docker-php-ext-enable memcached


#####################################
# PHP REDIS EXTENSION FOR PHP 7
#####################################

# Install Php Redis Extension
RUN pecl install -o -f redis \
    &&  rm -rf /tmp/pear \
    &&  docker-php-ext-enable redis
    

#####################################
# NodeJS
#####################################
RUN apt-get install -y \
        nodejs \
        npm \
    && ln -s /usr/bin/nodejs /usr/bin/node

# #####################################
# # Yarn
# #####################################
# RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
#     echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
#     apt-get update && apt-get install -y yarn

#####################################
# Grunt
#####################################
RUN npm i -g grunt-cli

#####################################
# Composer
#####################################
RUN curl -s https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer

#####################################
# FIX Apache
#####################################
RUN rm -R /etc/apache2/sites-enabled/

#####################################
# Coping files
#####################################
# Copy configration

ARG DOCKER_DIR=./docker/configs/application

COPY ${DOCKER_DIR}/opcache.ini /usr/local/etc/php/conf.d/opcache.ini
COPY ${DOCKER_DIR}/xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini
COPY ${DOCKER_DIR}/composer.json /root/.composer/auth.json

# Copy Magento files
RUN rm -rf          /var/www
COPY ./app          /var/www/app
RUN rm -rf          /var/www/app/design
RUN rm -rf          /var/www/app/code
COPY ./bin          /var/www/bin
COPY ./lib          /var/www/lib
COPY ./node_modules /var/www/node_modules
COPY ./phpserver    /var/www/phpserver
COPY ./pub          /var/www/pub
COPY ./vendor       /var/www/vendor
COPY ./var          /var/www/var

RUN chmod -R 777 /var/www
RUN mkdir /tmp/log

WORKDIR /var/www
